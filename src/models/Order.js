import mongoose from "mongoose";

const orderSchema = new mongoose.Schema({
    client: {
        id: String,
        name: String,
        email: String,
        phone: String,
        coodinates: {
            lat: String,
            lng: String,
        },
        street: String,
        town: String,
    },
    total: {
        type: Number,
        required: true
    },
    paymentMethod: {
        type: String,
        required: true
    },
    orderStatus: {
        type: String,
        default: "pending"
    },
    orderNumber: {
        type: String,
        required: true
    },
    products: [{
        _id: String,
        name:String,
        quantity: String
    }]

}, { timestamps: true });

module.exports = mongoose.model("Orders", orderSchema);
