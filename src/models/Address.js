import mongoose from 'mongoose'

// address schema
const addressSchema = new mongoose.Schema(
  {
    title: {
      type: String
    },
    street: {
      type: String
    },
    town: {
      type: String
    },
    coodinates: {
      lng: {
        type: String
      },
      lat: {
        type: String
      }
    }
  },
  { timestamps: true }
)
module.exports = mongoose.model('Addresses', addressSchema)
