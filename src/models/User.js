import mongoose from 'mongoose'
import Address from './Address'
import Order from './Order'
// user schema
const usersSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    phone: {
      type: String,
      required: true
    },
    avatar: {
      type: String,
      default: 'https://p1.hiclipart.com/preview/90/227/767/happy-face-emoji-smiley-wink-emoticon-yellow-facial-expression-nose-cheek-png-clipart.jpg'
    },
    password: {
      type: String,
      required: true,
      min: 4,
      max: 1024
    },
    roles: {
      type: String,
      default: 'client'
    },
    addresses: [{ type: mongoose.Schema.Types.ObjectId, ref: Address }],
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: Order }],
  },
  { timestamps: true }
)
module.exports = mongoose.model('Users', usersSchema)
