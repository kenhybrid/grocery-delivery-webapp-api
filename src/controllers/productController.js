// core plugins and models
import Product from "../models/Product";
import Category from "../models/Category";
// services
import {
  productCreateValidation,
  updateValidation,
  productSearchValidation,
  productfilterValidation,
} from "../schemas/validationschema/productsValidation";
const productDetails = async (req, res) => {
  /*
    algorithim
    -validate input
    -get product
    -print success

    @handle errors in catch
    */
  try {
    const { redis } = fastify;
    // create a redis key
    const key = req.url;
    const { id } = req.params;
    // cache
    await redis.get(key, async (err, data) => {
      if (err || data === null) {
        try {
          // get a product by id
          const product = await Product.findOne({ _id: id }).select(
            "name description short_description price size qty unit image_url category rating"
          );
          if (!product) throw "Product was not found.";
          // create a redis cashe
          redis.set(key, JSON.stringify({product:product}), "EX", 60 * 60 * 2, (err) => {
            if (err) throw err;
            // console.log("cacched successfully" + req.url)
          });
          // print success
          res.send({ product: product });
        } catch (error) {
          // error handling
          res.send({ error: error });
        }
      } else {
        res.send(JSON.parse(data));
        // update data in cache
        
      }
    });
     // get a product by id
     const product = await Product.findOne({ _id: id }).select(
      "name description short_description price size qty unit image_url category rating"
    );
    if (!product) throw "Product was not found.";
    // create a redis cashe
    redis.set(key, JSON.stringify({product:product}), "EX", 60 * 60 * 2, (err) => {
      if (err) throw err;
      // console.log("cacched successfully" + req.url)
    });
    // print success
    // res.send({ product: product });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const productsAll = async (req, res) => {
  /*
    algorithim
    -validate input
    -get all products in a paginated format
    -print success

    @handle errors in catch
    */
  try {
    const { redis } = fastify;
    // create a redis key
    const key = req.url;
    await redis.get(key, async (err, data) => {
      // check for error or missing cache

      if (err || data === null) {
        try {
          // use db to generate products and cache
          const limit = 6;
          const page =
            parseInt(req.params.page) >= 1 ? parseInt(req.params.page) : 1;
          const pagination = {};
          const endIndex = page * limit;
          const startIndex = (page - 1) * limit;
          const count = await Product.countDocuments().exec();
          let allPages;

          // getting all pages
          if ((await Product.countDocuments().exec()) % limit > 0) {
            allPages = parseInt(
              (await Product.countDocuments().exec()) / limit + 1
            );
          } else {
            allPages = (await Product.countDocuments().exec()) / limit;
          }
          // assigning pagination config
          pagination.pages = allPages;
          pagination.limit = limit;
          pagination.count = count;

          // pagination logic
          if (endIndex < count) {
            // show next pagination
            pagination.next = {
              page: page + 1,
            };
          }
          if (startIndex > 0) {
            pagination.previous = {
              page: page - 1,
            };
          }
          // get products from database
          const products = await Product.find()
            .select("name short_description price size qty unit image_url")
            .sort({ createdAt: "asc" })
            .limit(limit)
            .skip(startIndex);
          const payload = {
            products: products,
            pagination: pagination,
          };
          // create a redis cashe
          redis.set(key, JSON.stringify(payload), "EX", 60 * 60 * 2, (err) => {
            if (err) throw err;
            // console.log("cacched successfully" + req.url)
          });
          // print success
          res.send({ products: products, pagination });
        } catch (error) {
          // error handling
          res.send({ error: error });
        }
      } else {
        res.send(JSON.parse(data));
        // update data in cache
      }
    });
    // use db to generate products and cache
    const limit = 6;
    const page = parseInt(req.params.page) >= 1 ? parseInt(req.params.page) : 1;
    const pagination = {};
    const endIndex = page * limit;
    const startIndex = (page - 1) * limit;
    const count = await Product.countDocuments().exec();
    let allPages;

    // getting all pages
    if ((await Product.countDocuments().exec()) % limit > 0) {
      allPages = parseInt((await Product.countDocuments().exec()) / limit + 1);
    } else {
      allPages = (await Product.countDocuments().exec()) / limit;
    }
    // assigning pagination config
    pagination.pages = allPages;
    pagination.limit = limit;
    pagination.count = count;

    // pagination logic
    if (endIndex < count) {
      // show next pagination
      pagination.next = {
        page: page + 1,
      };
    }
    if (startIndex > 0) {
      pagination.previous = {
        page: page - 1,
      };
    }
    // get products from database
    const products = await Product.find()
      .select("name short_description price size qty unit image_url")
      .sort({ createdAt: "asc" })
      .limit(limit)
      .skip(startIndex);
    const payload = {
      products: products,
      pagination: pagination,
    };
    // create a redis cashe
    redis.set(key, JSON.stringify(payload), "EX", 60 * 60 * 2, (err) => {
      if (err) throw err;
      // console.log("cacched successfully" + req.url)
    });
    // print success
    //  res.send({ products: products, pagination });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const productUpdate = async (req, res) => {
  /*
    algorithim
    -validate input
    -get  products by and update
    -print success

    @handle errors in catch
    */
  try {
    const { id } = req.params;
    const {
      short_description,
      name,
      size,
      description,
      price,
      qty,
      unit,
      category,
      image_url,
    } = req.body;
    // validation
    const validData = await updateValidation(req.body);
    // check for availability of the product
    const product = await Product.findById({ _id: id });
    if (!product) throw "Product dosent exist.";

    // update a product
    const update = {
      name: name || product.name,
      description: description || product.description,
      short_description: short_description || product.short_description,
      price: price || product.price,
      qty: qty || product.qty,
      size: size || product.size,
      unit: unit || product.unit,
      image_url: image_url || product.image_url,
      category: category || product.category,
    };
    const updatedProduct = await Product.updateOne({ _id: id }, update);

    // update refrence in category incase category changed
    if (category) {
      if (product.category != category) {
        // remove product id from initial category
        const categoryProductRemoval = await Category.updateOne(
          { _id: product.category },
          { $pull: { products: product._id } }
        );
        // push product in new category
        const categoryPopulation = await Category.updateOne(
          { _id: category },
          { $push: { products: product._id } }
        );
      }
    }

    // print success
    res.send({ message: "Product has been updated." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const productCreate = async (req, res) => {
  /*
    algorithim
    -validate input
    -create product
    -print success

    @handle errors in catch
    */
  try {
    const {
      short_description,
      name,
      description,
      price,
      qty,
      size,
      unit,
      category,
      image_url,
    } = await req.body;
    // validation
    const validData = await productCreateValidation(req.body);
    // create a product and save
    const newProduct = await new Product({
      name: name,
      description: description,
      short_description: short_description,
      price: price,
      qty: qty,
      unit: unit,
      size: size,
      category: category,
      image_url: image_url || null,
    });

    const product = await newProduct.save();
    // get the product category and push it into category products
    const categoryPopulation = await Category.updateOne(
      { _id: product.category },
      { $push: { products: product._id } }
    );
    // on success
    res.send({ message: "Product has been created." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const productDelete = async (req, res) => {
  /*
    algorithim
    -validate input
    -use mongoose middlewears to delete associated collections in categories collection
    -print success

    @handle errors in catch
    */
  try {
    const { id } = req.params;
    // get product by id and pull id from category
    const product = await Product.findOne({ _id: id });
    if (!product) throw "No product with such an Id";

    // pull from category and update
    const category = await Category.updateOne(
      { _id: product.category },
      { $pull: { products: product._id } }
    );

    // delete product
    const deleteProduct = await Product.deleteOne({ _id: id });
    res.send({ message: "Product is deleted." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const productSearch = async (req, res) => {
  /*
    algorithim
    -validate input
    -get product by name , category ,description
    -print success

    @handle errors in catch
    */
  try {
    const { searchTerm } = await req.body;
    // validation
    const validData = await productSearchValidation(req.body);
    //  search a product
    const products = await Product.find({
      $or: [
        { name: new RegExp(searchTerm, "gi") },
        { short_description: new RegExp(searchTerm, "gi") },
      ],
    }).select("name short_description size rating price unit image_url");
    // on success
    res.send({ products: products });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const productFilter = async (req, res) => {
  /*
    algorithim
    -validate input
    -filter by price,units,category
    -print success

    @handle errors in catch
    */
  try {
    const validData = await productfilterValidation(req.body);
    const { unit, category, searchTerm } = await req.body;
    //  search and filter product
    if (!category && !unit) {
      // search without category or units
      const products = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      }).select("name short_description size rating price unit image_url");
      // on success
      res.send({ products: products });
    } else if (!category) {
      // search without category
      const products = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      })
        .where("unit")
        .equals(unit)
        .select("name short_description size rating price unit image_url");
      // on success
      res.send({ products: products });
    } else if (!unit) {
      // search without  units
      const products = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      })
        .where("category")
        .equals(category)
        .select("name short_description size rating price unit image_url");
      // on success
      res.send({ products: products });
    } else {
      // search with category and units
      const products = await Product.find({
        $or: [
          { name: new RegExp(searchTerm, "gi") },
          { short_description: new RegExp(searchTerm, "gi") },
        ],
      })
        .where("category")
        .equals(category)
        .where("unit")
        .equals(unit)
        .select("name short_description size rating price unit image_url");
      // on success
      res.send({ products: products });
    }
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};
module.exports = {
  productsAll,
  productUpdate,
  productCreate,
  productDelete,
  productSearch,
  productDetails,
  productFilter,
};
