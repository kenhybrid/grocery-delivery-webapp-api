import Address from "../models/Address";
import User from "../models/User";

import {
  addressCreateValidation,
  addressUpdateValidation,
} from "../schemas/validationschema/addressValidation";
const addressCreate = async (req, res) => {
  /*
      algorithim
      -validate input
      -create address
      -print success
  
      @handle errors in catch
      */
  try {
    // validation
    const validData = await addressCreateValidation(req.body);
    const { userId, title, street, town, coodinates } = await req.body;
    // create address and save
    const newAddress = await new Address({
      title: title,
      town: town,
      street: street,
      coodinates: coodinates,
    });
    const address = await newAddress.save();
    // push address id to  user
    const user = await User.updateOne(
      { _id: userId },
      { $push: { addresses: address._id } }
    );
    // print success
    res.send({ message: "Address has been created." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const addressDelete = async (req, res) => {
  /*
        algorithim
        -validate input
        -get address by id and unlink from user then delete
        -print success
    
        @handle errors in catch
        */
  try {
    const { id } = req.params;
    const { userId } = req.body;
    // find the address by id
    const address = await Address.findOne({ _id: id });
    if (!address) throw "The address does not exist.";
    // unlink from use
    const userAddress = await User.updateOne(
      { _id: userId },
      { $pull: { addresses: address._id } }
    );
    const deleteCategory = await Address.deleteOne({ _id: id });
    // print success
    res.send({ message: "Address has been deleted." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};
const addressUpdate = async (req, res) => {
  /*
      algorithim
      -validate input
      -getaddress by id and update
      -print success
  
      @handle errors in catch
      */
  try {
    //   validation
    const validData = await addressUpdateValidation(req.body)

    const { id } = req.params;
    const { title, street, town, coodinates } = await req.body;
    // find address
    const address = await Address.findOne({ _id: id });
    if (!address) throw "The address does not exist.";
    // update address and save
    const update = {
      title: title,
      town: town || address.town,
      street: street || address.street,
      coodinates: coodinates || address.coodinates,
    };
    const updatedAddress = await Address.updateOne({ _id: id }, update);

    // print success
    res.send({ message: "Address has been updated." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

module.exports = {
  addressUpdate,
  addressCreate,
  addressDelete,
};
