// core models and plugins
import argon from "argon2";
import User from "../models/User";
import Address from "../models/Address";
// services
import { wellcomeMail, passwordResetMail } from "../services/mailerService";
import Randomize from "../services/randomizeService";
import {
  loginValidation,
  registerValidation,
  updateValidation,
  passwordResetValidation,
} from "../schemas/validationschema/usersValidation";

const userDetails = async (req, res) => {
  /*
    algorithim
    -validate input
    -populate the user and address collections
    -print success

    @handle errors in catch
    */
  try {
    const { id } = req.params;
    //  find one user by id
    const user = await User.findById({ _id: id })
      .select("name phone email createdAt")
      .populate("address");
    if (!user) throw "Email is not registered.";

    // print success
    res.send({ user: user });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const userCurrent = async (req, res) => {
  /*
-get user from header
    -populate details
    -print success
  */
  try {
    // get id from token
    const { id } = req.user;
    // populate all fields
    const user = await User.findOne({ _id: id })
      .select("name email phone avatar roles")
      .populate("addresses", "title street town coodinates createdAt")
      .populate(
        "orders",
        "total paymentMethod orderStatus orderNumber products"
      );
    // print success
    res.send({ user: user });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};
const userLogin = async (req, res) => {
  /*
    algorithim
    -validate input
    -check if email exists(throw error if not and proceed if)
    -compare the hashed password from db
    -then login the user the user
    -create a jwt token
    -print success

    @handle errors in catch
    */
  try {
    const { email, password } = await req.body;

    const validData = await loginValidation(req.body);

    // check for already registered user with same email
    const user = await User.findOne({ email: email }).select(
      "name email phone password"
    );
    if (!user) throw "Email is not registered.";

    // compare passwords
    const match = await argon.verify(user.password, password);
    if (!match) throw "Authentication failed.";

    // token generation
    const token = await fastify.jwt.sign({
      id: user._id,
      name: user.name,
      email: user.email,
      phone: user.phone,
    });

    // success
    res.send({ token: token });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const userRegistration = async (req, res) => {
  /*
    algorithim
    -validate input
    -check for already registered email(throw error if and proceed if not)
    -hash the password
    -then create the user
    -send a welcome email
    -print success

    @handle errors in catch
    */
  try {
    const { email, name, phone, password, roles } = req.body;
    // validate data
    const validData = await registerValidation(req.body);
    // check for already registered user with same email
    const user = await User.findOne({ email: email });
    if (user) throw "Email is already in use.";

    // then hash the passsword
    const hash = await argon.hash(password);
    if (!hash) throw "Failed to hash password.";

    // create a user
    const doc = new User({
      name: name,
      email: email,
      phone: phone,
      roles: roles,
      password: hash,
    });
    const result = await doc.save();

    // send a welcome email
    const data = {
      name: result.name,
      email: result.email,
    };
    // const emailSent = await wellcomeMail(data);
    // console.log(emailSent)
    //  check the status of the mail to accert that its sent

    /* ? */
    // on success
    res.send({ message: "User has been created." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const userUpdate = async (req, res) => {
  /*
    algorithim
    -validate input
    -hash the password
    -then update the user
    -print success

    @handle errors in catch
    */
  try {
    const { id } = await req.params;
    const { email, name, phone, password, roles } = req.body;
    // validation
    const validData = await updateValidation(req.body);
    // check for availability of the email
    const user = await User.findOne({ _id: id });
    if (!user) throw "User dosent exist.";

    // then hash the passsword
    const hash = await argon.hash(password);
    if (!hash) throw "Failed to hash password.";

    // update a user
    const update = {
      name: name || user.name,
      email: email || user.email,
      phone: phone || user.phone,
      roles: roles || user.roles,
      password: hash || user.password,
    };
    const result = await User.updateOne({ _id: id }, update);

    // on success
    res.send({ message: "User has been updated." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const userDelete = async (req, res) => {
  /*
    algorithim
    -validate input
    -use mongoose middlewears to delete associated collections in address collection
    -print success

    @handle errors in catch
    */
  try {
    const { id } = await req.param;
    //  find user
    const user = await User.findOne({ _id: id });
    if (!user) throw "User does not exist";

    // delete addreses
    const addressDelete = await Address.deleteMany({
      _id: { $in: user.addresses },
    });
    // delete user
    const deleteUser = await User.deleteOne({ _id: id });
    // on success
    res.send({ message: "User has been deleted" });
  } catch (error) {
    //  error handler
    res.send({ error: error });
  }
};

const passwordReset = async (req, res) => {
  /*
    algorithim
    -validate input
    -check if email exists(throw error if not and proceed if)
    -generate a password and update db
    -then mail the password to the client
    -print success

    @handle errors in catch
    */
  try {
    const { email } = await req.body;
    // validation
    const validData = await passwordResetValidation(req.body);
    // check for already registered user with same email
    const user = await User.findOne({ email: email }).select("name password");
    if (!user) throw "Email is not registered.";

    // generate a random passwords
    const password = await Randomize(6);

    // hash password for update
    const hash = await argon.hash(password);
    if (!hash) throw "Failed to hash password.";

    // update password
    const result = await User.updateOne({ email: email }, { password: hash });

    // send password reset mail
    /* ? */
    const data = {
      name: user.name,
      email: user.email,
      password: password,
    };
    // const emailSent = await passwordResetMail(data);
    // console.log(emailSent);
    // // success
    res.send({ user: user });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

module.exports = {
  userLogin,
  userRegistration,
  userUpdate,
  userDelete,
  passwordReset,
  userDetails,
  userCurrent,
};
