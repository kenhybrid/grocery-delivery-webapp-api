import Category from "../models/Category";
import Product from "../models/Product";
import {
  categoryCreateValidation,
  categoryUpdateValidation,
} from "../schemas/validationschema/categoryValidation";
const categorySort = async (req, res) => {
  /*
    algorithim
    -validate input
    -get categories and populate products
    -print success

    @handle errors in catch
    */
  try {
    const { redis } = fastify;
    const key = req.url;
    const { id } = req.params;

    // check for error or null cache data
    await redis.get(key, async (err, data) => {
      if (err || data === null) {
        try {
          // get category by id and populate products
          const categoryProducts = await Category.findOne({ _id: id })
            .select("name")
            .populate("products", "name short_description size price unit");
          if (!categoryProducts) throw "Category not found";
          // set cache

          redis.set(
            key,
            JSON.stringify({categoryProducts:categoryProducts}),
            "EX",
            60 * 60 * 2,
            (err) => {
              if (err) throw err;
            }
          );
          // print success
          res.send({ categoryProducts: categoryProducts });
        } catch (error) {
          // error handling
          res.send({ error: error });
        }
      } else {
        res.send(JSON.parse(data));
        // update data in cache
      }
    });
    // get category by id and populate products
    const categoryProducts = await Category.findOne({ _id: id })
      .select("name")
      .populate("products", "name short_description size price unit");
    if (!categoryProducts) throw "Category not found";
    // set cache

    redis.set(
      key,
      JSON.stringify({categoryProducts:categoryProducts}),
      "EX",
      60 * 60 * 2,
      (err) => {
        if (err) throw err;
      }
    );
    // res.send({ category: categoryProducts });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const categoryCreate = async (req, res) => {
  /*
      algorithim
      -validate input
      -create a category
      -print success
  
      @handle errors in catch
      */
  try {
    const { name } = req.body;
    // validation
    const validData = await categoryCreateValidation(req.body);
    // create a category
    const newCategory = await new Category({
      name: name,
    });
    const category = await newCategory.save();
    // print success
    res.send({ message: "Category has been created." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};
const categoryUpdate = async (req, res) => {
  /*
      algorithim
      -validate input
      -create a category
      -print success
  
      @handle errors in catch
      */
  try {

    const { id } = req.params;
    const { name } = req.body;
    // validation
    const validData = await categoryUpdateValidation(req.body);
    // create a category
    const category = await Category.findOne({ _id: id });
    if (!category) throw "Category not found";

    const update = await Category.updateOne({ _id: id }, { name: name || category.name });
    // print success
    res.send({ message: "Category has been updated." });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};
const categoriesAll = async (req, res) => {
  /*
    algorithim
    -validate input
    -get all categories in a paginated format
    -print success

    @handle errors in catch
    */
  try {
    const { redis } = fastify;
    const key = req.url;
    // check for error or null cache data
    await redis.get(key, async (err, data) => {
      if (err || data === null) {
        try {
          // find all categories
          const categories = await Category.find().select(
            "name image_url products"
          );
          if (categories.length < 1) throw "There are no categories.";
          redis.set(
            key,
            JSON.stringify({ categories: categories }),
            "EX",
            60 * 60 * 2,
            (err) => {
              if (err) throw err;
            }
          );
          // print success
          res.send({ categories: categories });
        } catch (error) {
          // error handling
          res.send({ error: error });
        }
      } else {
        res.send(JSON.parse(data));
        // update data in cache
      }
    });
    // find all categories
    const categories = await Category.find().select("name image_url products");
    if (categories.length < 1) throw "There are no categories.";
    redis.set(
      key,
      JSON.stringify({ categories: categories }),
      "EX",
      60 * 60 * 2,
      (err) => {
        if (err) throw err;
      }
    );
    // print success
    // res.send({ categories: categories });
  } catch (error) {
    // error handling
    res.send({ error: error });
  }
};

const categoryDelete = async (req, res) => {
  /*
      algorithim
      -validate input
      -delete a category
      -print success
  
      @handle errors in catch
      */
  try {
    const { id } = await req.params;
    //  find user
    const category = await Category.findOne({ _id: id });
    if (!category) throw "Category does not exist";

    // delete addreses
    if (category.products.length >= 1) {
      const productDelete = await Product.deleteMany({
        _id: { $in: category.products },
      });
    }

    // delete user
    const deleteCategory = await Category.deleteOne({ _id: id });
    // on success
    res.send({ message: "Category has been deleted" });
  } catch (error) {
    //  error handler
    res.send({ error: error });
  }
};

module.exports = {
  categoriesAll,
  categorySort,
  categoryCreate,
  categoryDelete,
  categoryUpdate,
};
