import Order from "../models/Order"
import Randomize from "../services/randomizeService"
import User from "../models/User"

const orderCreate = async (req, res) => {
    /*
      algorithim
      -validate input
      -create order
      -update user reference or relation
      -print success
  
      @handle errors in catch
      */
    try {
        const { client, total, paymentMethod, products } = await req.body;
        // create an order number
        const orderNumber = await Randomize(6);

        // check payment method to set order status
        if (paymenthmethod === "payOnDelivery") {
            // create order and save
            const newOrder = await new Order({
                client: client,
                total: total,
                paymentMethod: paymentMethod,
                orderNumber: orderNumber,
                products: products,
            })
            const order = await newOrder.save()
            // push to user orders
            const userPopulation = await User.updateOne({ _id: client.id }, { $push: { orders: order._id } })

            // print success
            res.send({ order: order });
        } else if (paymenthmethod === "lipaNaMpesa") {
            // create order and save
            const newOrder = await new Order({
                client: client,
                total: total,
                paymentMethod: paymentMethod,
                orderStatus: "awaitingPayment",
                orderNumber: orderNumber,
                products: products,
            })
            const order = await newOrder.save()
            // push to user orders
            const userPopulation = await User.updateOne({ _id: client.id }, { $push: { orders: order._id } })
            // print success
            res.send({ order: order });
        } else if (paymenthmethod === "stripe") {
            // create order and save
            const newOrder = await new Order({
                client: client,
                total: total,
                paymentMethod: paymentMethod,
                orderStatus: "awaitingPayment",
                orderNumber: orderNumber,
                products: products,
            })
            const order = await newOrder.save()
            // push to user orders
            const userPopulation = await User.updateOne({ _id: client.id }, { $push: { orders: order._id } })
            // print success
            res.send({ order: order });
        } else {
            res.send({ message: "Choose a payment method." });

        }

    } catch (error) {
        // error handling
        res.send({ error: error });
    }
};

const orderByUser = async (req, res) => {
    /*
        algorithim
        -validate input
        -get all orders by a user
        -print success
    
        @handle errors in catch
        */
    try {
        const { id } = req.params;
        // get orders from database
        const orders = await User.find({_id:id})
            .select("name")
            .populate("orders", " client total paymentMethod orderStatus orderNumber createdAt");

        // print success
        res.send({ orders: orders });
    } catch (error) {
        // error handling
        res.send({ error: error });
    }
};
const orderAll = async (req, res) => {
    /*
        algorithim
        -validate input
        -get all orders
        -print success
    
        @handle errors in catch
        */
    try {
        const limit = 6;
        const page = parseInt(req.params.page) >= 1 ? parseInt(req.params.page) : 1;
        const pagination = {};
        const endIndex = page * limit;
        const startIndex = (page - 1) * limit;
        const count = await Order.countDocuments().exec();
        let allPages;

        // getting all pages
        if ((await Order.countDocuments().exec()) % limit > 0) {
            allPages = parseInt((await Order.countDocuments().exec()) / limit + 1);
        } else {
            allPages = (await Order.countDocuments().exec()) / limit;
        }
        // assigning pagination config
        pagination.pages = allPages;
        pagination.limit = limit;
        pagination.count = count;

        // pagination logic
        if (endIndex < count) {
            // show next pagination
            pagination.next = {
                page: page + 1,
            };
        }
        if (startIndex > 0) {
            pagination.previous = {
                page: page - 1,
            };
        }
        // get orders from database
        const orders = await Order.find()
            .select("client total paymentMethod orderStatus orderNumber products createdAt")
            .sort({ createdAt: "asc" })
            .limit(limit)
            .skip(startIndex);

        // print success
        res.send({ orders: orders, pagination });
    } catch (error) {
        // error handling
        res.send({ error: error });
    }
};
const orderSort = async (req, res) => {
    /*
      algorithim
      -validate input
      -sort ordes as per status
      -print success
  
      @handle errors in catch
      */
    try {
        const { orderStatus } = await req.body;
        // find order by status
        const orders = await Order.find({ orserStatus: orderStatus }).sort({ createdAt: "asc" })
        // print success
        res.send({ orders: orders });
    } catch (error) {
        // error handling
        res.send({ error: error });
    }
};

const orderCancel = async (req, res) => {
    /*
      algorithim
      -validate input
      -find and update an order
      -print success
  
      @handle errors in catch
      */
    try {
        const { id } = req.params;
        // find order by status
        const order = await Order.findOne({ _id: id })
        if (!order) throw "No order Found";
        // order update status
        const update = {
            orderStatus: "canceled"
        }
        const updateOrder = await Order.updateOne({ _id: order._id }, update)
        // print success
        res.send({ message: "Order has been canceled" });
    } catch (error) {
        // error handling
        res.send({ error: error });
    }
};
module.exports = {
    orderSort,
    orderCreate,
    orderAll,
    orderByUser,
    orderCancel
};
