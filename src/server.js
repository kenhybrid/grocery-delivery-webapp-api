// importing core plugins
import Fastify from "fastify";
import Cors from "fastify-cors";
import Helmet from "fastify-helmet";
import Fastifystatic from "fastify-static";
import Template from "point-of-view";
import Compress from "fastify-compress";
import Jwt from "fastify-jwt";
import Redis from "fastify-redis";
import path from "path";
import { JWT, APP, REDIS } from "./config";
// initializing fastify
const fastify = Fastify({ logger: true });

// core plugins
fastify.register(Cors, { origin: "*" });
fastify.register(Helmet);
fastify.register(Compress);
fastify.register(Redis, {
  host: REDIS.host,
  port: REDIS.port,
  closeClient: true,
});
fastify.register(Fastifystatic, {
  root: path.join(__dirname, "../public"),
});
fastify.register(Jwt, {
  secret: JWT.secret,
  sign: {
    expiresIn:JWT.expiresIn,
  },
});
fastify.register(Template, {
  engine: {
    ejs: require("ejs"),
  },
});

// custom plugins
fastify.register(require("./loaders/mongoose"));
fastify.setNotFoundHandler((req, res) => {
  res.sendFile("index.html");
});

// decorators hooks and middlewears
// making fastify global
global.fastify = fastify;
fastify.register(require("./middlewears/authMiddlewear"));
// services and routes
fastify.register(
  async (openRoutes) => openRoutes.register(require("./routes/userRoute")),
  { prefix: "/api/user" }
);
fastify.register(
  async (openRoutes) => openRoutes.register(require("./routes/productRoute")),
  { prefix: "/api/product" }
);
fastify.register(
  async (openRoutes) => openRoutes.register(require("./routes/categoryRoute")),
  { prefix: "/api/category" }
);
fastify.register(
  async (openRoutes) => openRoutes.register(require("./routes/addressRoute")),
  { prefix: "/api/address" }
);
fastify.register(
  async (openRoutes) => openRoutes.register(require("./routes/orderRoute")),
  { prefix: "/api/order" }
);

fastify.get("/me",async (req,res)=>{
  res.send({se:"kk"})
})
// serving app
const start = async () => {
  try {
    await fastify.ready();
    await fastify.listen(APP.port);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();

module.exports = fastify;
