import mongoose from 'mongoose'
import { DB } from '../config'

export default async (fastify, options, done) => {
  try {
    await mongoose.connect(DB.uri, DB.config)
    fastify.log.info('Database is Connected')
    done()
  } catch (error) {
    fastify.log.error('Database Error:' + error)
    done(error)
  }
}
