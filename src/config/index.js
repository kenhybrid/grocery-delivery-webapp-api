require("dotenv").config();
const DB = {
  uri: process.env.MONGO_URI,
  config: {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  },
};

const JWT = {
  secret: process.env.SECRET,
  expiresIn: process.env.EXPIRERY,
};

const APP = {
  port: process.env.PORT,
  name: process.env.NAME,
};
const REDIS = {
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
};
const MAILER = {
  email: process.env.EMAIL,
  password: process.env.PASSWORD,
  host: process.env.HOST,
  port: process.env.MAILER_PORT,
  secure: process.env.SECURE,
};
module.exports = {
  DB,
  APP,
  JWT,
  MAILER,
  REDIS,
};
