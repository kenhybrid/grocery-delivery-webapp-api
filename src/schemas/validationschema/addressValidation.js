import Joi from "@hapi/joi";

const addressCreateValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    userId: Joi.string().required(),
    title: Joi.string().required(),
    street: Joi.string().required(),
    town: Joi.string().required(),
    coodinates: Joi.object()
      .keys({
        lat: Joi.string().required(),
        lng: Joi.string().required(),
      })
      .required(),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

const addressUpdateValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    userId: Joi.string().required(),
    title: Joi.string(),
    street: Joi.string(),
    town: Joi.string(),
    coodinates: Joi.object().keys({
      lat: Joi.string(),
      lng: Joi.string(),
    }),
  });
  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

module.exports = {
  addressCreateValidation,
  addressUpdateValidation,
};
