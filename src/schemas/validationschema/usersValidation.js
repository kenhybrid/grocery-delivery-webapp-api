import Joi from "@hapi/joi";

const loginValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .max(50)
      .required(),
    password: Joi.string()
      .trim()
      .min(4)
      .max(16)
      .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
      .required(),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

const registerValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    name: Joi.string().min(3).max(25).required(),
    roles: Joi.string().min(3).max(8).required(),
    phone: Joi.string().min(9).max(13).required(),
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .max(30)
      .required(),
    password: Joi.string()
      .trim()
      .min(4)
      .max(16)
      .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
      .required(),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

const updateValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    name: Joi.string().min(3).max(25),
    roles: Joi.string().min(3).max(8),
    phone: Joi.string().min(9).max(13),
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .max(30),
    password: Joi.string()
      .trim()
      .min(4)
      .max(16)
      .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};
const passwordResetValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .max(30)
      .required(),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

module.exports = {
  loginValidation,
  registerValidation,
  updateValidation,
  passwordResetValidation,
};
