import Joi from "@hapi/joi";

const categoryCreateValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    name: Joi.string().min(3).max(50).required(),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

const categoryUpdateValidation = async (data) => {
  // validation schema
  const schema = Joi.object({
    name: Joi.string().min(3).max(50),
  });

  // validate
  const validData = await Joi.attempt(data, schema);
  // if (!validData) throw "Input vaid data";
};

module.exports = {
  categoryCreateValidation,
  categoryUpdateValidation,
};
