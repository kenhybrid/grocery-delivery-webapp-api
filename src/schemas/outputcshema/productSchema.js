const allProductsSchema = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          products: {
            type: "object",
            items: {
              type: "object",
              properties: {
                _id: { type: "string" },
                name: { type: "string" },
                short_description: { type: "string" },
                unit: { type: "string" },
                qty: { type: "integer" },
                size: { type: "string" },
                price: { type: "integer" },
                image_url: { type: "string" },
              },
            },
          },
          error: { type: "string" },
          pagination: {
            type: "object",
            properties: {
              limit: { type: "number" },
              count: { type: "number" },
              pages: { type: "number" },
              next: {
                type: "object",
                properties: {
                  page: { type: "number" },
                },
              },
              previous: {
                type: "object",
                properties: {
                  page: { type: "number" },
                },
              },
            },
          },
        },
      },
    },
  },
};

const productCreateSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const productUpdateSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const productDeleteSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const productSearchSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        products: {
          type: "array",
          items: {
            type: "object",
            properties: {
              _id: { type: "string" },
              name: { type: "string" },
              short_description: { type: "string" },
              unit: { type: "string" },
              qty: { type: "integer" },
              size: { type: "string" },
              price: { type: "integer" },
              image_url: { type: "string" },
            },
          },
        },
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const productFilterSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        products: {
          type: "array",
          items: {
            type: "object",
            properties: {
              _id: { type: "string" },
              name: { type: "string" },
              short_description: { type: "string" },
              unit: { type: "string" },
              qty: { type: "integer" },
              size: { type: "string" },
              price: { type: "integer" },
              image_url: { type: "string" },
            },
          },
        },
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const oneProductSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        product: {
          type: "object",

          properties: {
            _id: { type: "string" },
            name: { type: "string" },
            short_description: { type: "string" },
            description: { type: "string" },
            unit: { type: "string" },
            qty: { type: "integer" },
            size: { type: "string" },
            rating: { type: "number" },
            price: { type: "integer" },
            image_url: { type: "string" },
          },
        },
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
module.exports = {
  allProductsSchema,
  productDeleteSchema,
  productUpdateSchema,
  productCreateSchema,
  productSearchSchema,
  productFilterSchema,
  oneProductSchema,
};
