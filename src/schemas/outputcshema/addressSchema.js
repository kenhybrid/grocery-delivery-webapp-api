const addressCreatechema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

const addressUpdateSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string" },
    },
  },
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

const addressDeleteSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string" },
    },
  },
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

module.exports = {
  addressDeleteSchema,
  addressUpdateSchema,
  addressCreatechema,
};
