const loginSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        token: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

const registerSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

const userUpdateSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string" },
    },
  },
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const userDeleteSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string" },
    },
  },
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
module.exports = {
  registerSchema,
  loginSchema,
  userDeleteSchema,
  userUpdateSchema,
};
