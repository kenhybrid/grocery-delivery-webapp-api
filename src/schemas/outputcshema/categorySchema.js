const allCategoriesSchema = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          categories: {
            type: "array",
            items: {
              type: "object",
              properties: {
                _id: { type: "string" },
                name: { type: "string" },
                products: {
                  type: "array",
                },
                image_url: { type: "string" },
              },
            },
          },
          error: { type: "string" },
        },
      },
    },
  },
};

const createCategorySchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const updateCategorySchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};
const categoryDeleteSchema = {
  response: {
    200: {
      type: "object",
      properties: {
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

const sortCategorySchema = {
  response: {
    200: {
      type: "object",
      properties: {
        categoryProducts: {
          type: "object",
          properties: {
            _id: { type: "string" },
            name: { type: "string" },
            products: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  _id: { type: "string" },
                  name: { type: "string" },
                  short_description: { type: "string" },
                  unit: { type: "string" },
                  qty: { type: "integer" },
                  size: { type: "string" },
                  price: { type: "integer" },
                  image_url: { type: "integer" },
                },
              },
            },
          },
        },
        message: { type: "string" },
        error: { type: "string" },
      },
    },
  },
};

module.exports = {
  allCategoriesSchema,
  createCategorySchema,
  sortCategorySchema,
  categoryDeleteSchema,
  updateCategorySchema,
};
