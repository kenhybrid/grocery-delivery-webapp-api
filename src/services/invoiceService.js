import puperteer from 'puperteer'

const orderInvoice = async (data) => {
  await joi.validate(data)
}

const rentInvoice = async (data) => {
  await joi.validate(data)
}

const introductionInvoice = async (data) => {
  await joi.validate(data)
}

module.exports = {
  orderInvoice,
  rentInvoice,
  introductionInvoice
}
