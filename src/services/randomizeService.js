import fp from "fastify-plugin"
module.exports = fp((length) => {
    try {
        var payload = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var result = '';
        for (var i = 0; i < length; i++) {
            result +=  payload.charAt(Math.floor(Math.random() * payload.length));
        }
        return result;
    } catch (error) {

    }

})

