"use strict";
import nodemailer from "nodemailer";
import { APP, MAILER } from "../config";

// async..await is not allowed in global scope, must use a wrapper
const wellcomeMail = async (data) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: MAILER.host,
        port: MAILER.port,
        secure: MAILER.secure, // true for 465, false for other ports
        auth: {
            user: MAILER.email,
            pass: MAILER.password,
        },
    });
    const message = {
        from: APP.name + `<${MAILER.email}>`,
        to: data.email,
        subject: `WELLCOME TO ${APP.name} ${data.name}`,
        html: `<body>
              <h3>Dear ${data.name}</h3>
            </body> `,
    };
    // send mail with defined transport object
    let mailStatus = await transporter.sendMail(message);
    return mailStatus;

}
const newsLettersMail = async (data) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: MAILER.host,
        port: MAILER.port,
        secure: MAILER.secure, // true for 465, false for other ports
        auth: {
            user: MAILER.email,
            pass: MAILER.password,
        },
    });
    const message = {
        from: APP.name + `<${MAILER.email}>`,
        to: data.email,
        subject: `WELLCOME TO ${APP.name} ${data.name}`,
        html: `<body>
              <h3>Dear ${data.name}</h3>
            </body> `,
    };
    // send mail with defined transport object
    let mailStatus = await transporter.sendMail(message);
    return mailStatus;

}
const passwordResetMail = async (data) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: MAILER.host,
        port: MAILER.port,
        secure: MAILER.secure, // true for 465, false for other ports
        auth: {
            user: MAILER.email,
            pass: MAILER.password,
        },
    });
    const message = {
        from: APP.name + `<${MAILER.email}>`,
        to: data.email,
        subject: `WELLCOME TO ${APP.name} ${data.name}`,
        html: `<body>
              <h3>Dear ${data.name}</h3>
              <p>Your new password is: <b>${data.password}</b></p>
            </body> `,
    };
    // send mail with defined transport object
    let mailStatus = await transporter.sendMail(message);
    return mailStatus;

}
module.exports = {
  wellcomeMail,
  passwordResetMail,
  newsLettersMail,
};
