const fp = require('fastify-plugin')

module.exports = fp(async (fastify, options, done) => {
  await fastify.decorate('authenticate', async (req, res) => {
    try {
      await req.jwtVerify()
    } catch (err) {
      res.send(err)
    }
  })
})
