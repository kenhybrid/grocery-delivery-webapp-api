import {
    orderSort,
    orderCreate,
    orderAll,
    orderByUser,
    orderCancel
} from '../controllers/orderController'

const orderRoute = async (fastify, opts, done) => {
    // get all orders
    fastify.route({
        method:"GET",
        url:"/",
        handler:orderAll
    })
    // create order route
    fastify.route({
        method: 'POST',
        url: '/create',
        handler: orderCreate
    })
    // order orders by user
     fastify.route({
        method: 'POST',
        url: '/orderbyuser/:id',
        handler: orderByUser
    })
    //   cancel order route
    fastify.route({
        method: 'PUT',
        url: '/update/:id',
        handler: orderCancel
    })
    //   sort route
    fastify.route({
        method: 'POST',
        url: '/sort',
        handler: orderSort
    })
    done()
}

module.exports = orderRoute
