import {
  productsAll,
  productUpdate,
  productCreate,
  productDelete,
  productSearch,
  productDetails,
  productFilter,
} from "../controllers/productController";
import {
  allProductsSchema,
  productCreateSchema,
  productUpdateSchema,
  productDeleteSchema,
  productSearchSchema,
  productFilterSchema,oneProductSchema
} from "../schemas/outputcshema/productSchema";
const productsRoute = async (fastify, opts, done) => {
  // get all products
  fastify.route({
    schema: allProductsSchema,
    method: "GET",
    url: "/:page",
    // preValidation: fastify.authenticate,
    handler: productsAll,
  });
  // product details route
  fastify.route({
    schema:oneProductSchema,
    method: "GET",
    url: "/details/:id",
    //   preValidation: fastify.authenticate,
    handler: productDetails,
  });
  // create products route
  fastify.route({
    schema: productCreateSchema,
    method: "POST",
    url: "/create",
    handler: productCreate,
  });
  //   search product route
  fastify.route({
    schema:productSearchSchema,
    method: "POST",
    url: "/search",
    handler: productSearch,
  });
  //  product delete route
  fastify.route({
    schema:productDeleteSchema,
    method: "DELETE",
    url: "/delete/:id",
    handler: productDelete,
  });
  //   update route
  fastify.route({
    schema: productUpdateSchema,
    method: "PUT",
    url: "/update/:id",
    handler: productUpdate,
  });
  fastify.route({
    schema:productFilterSchema,
    method: "POST",
    url: "/filter",
    handler: productFilter,
  });
  done();
};

module.exports = productsRoute;
