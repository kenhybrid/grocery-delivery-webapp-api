import {
  userLogin,
  userRegistration,
  userUpdate,
  userDelete,
  passwordReset,
  userDetails,
  userCurrent,
} from "../controllers/userController";
import {
  loginSchema,
  registerSchema,
  userUpdateSchema,
  userDeleteSchema
} from "../schemas/outputcshema/userSchema";
// import Joi from "@hapi/joi"
const usersRoute = async (fastify, opts, done) => {
  // login route
  fastify.route({
    schema: loginSchema,
    method: "POST",
    url: "/login",
    handler: userLogin,
  });
  //current user route
  fastify.route({
    method: "GET",
    url: "/currentuser",
    preValidation: fastify.authenticate,
    handler: userCurrent,
  });
  // userdetails route
  fastify.route({
    method: "GET",
    url: "/details",
    preValidation: fastify.authenticate,
    handler: userDetails,
  });
  //   register route
  fastify.route({
    method: "POST",
    url: "/register",
    handler: userRegistration,
    schema: registerSchema,
  });
  //   update route
  fastify.route({
    schema: userUpdateSchema,
    method: "PUT",
    url: "/update/:id",
    handler: userUpdate,
  });
  //   delete route
  fastify.route({
    schema:userDeleteSchema,
    method: "DELETE",
    url: "/delete/:id",
    handler: userDelete,
  });
  //   reset route
  fastify.route({
    method: "POST",
    url: "/reset",
    handler: passwordReset,
  });
  done();
};

module.exports = usersRoute;
