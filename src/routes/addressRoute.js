import {
  addressUpdate,
  addressCreate,
  addressDelete,
} from "../controllers/addressController";

import {
  addressDeleteSchema,
  addressUpdateSchema,
  addressCreatechema,
} from "../schemas/outputcshema/addressSchema";

const addressRoute = async (fastify, opts, done) => {
  // CREATE route
  fastify.route({
    schema: addressCreatechema,
    method: "POST",
    url: "/create",
    handler: addressCreate,
  });
  //   update route
  fastify.route({
    schema: addressUpdateSchema,
    method: "PUT",
    url: "/update/:id",
    handler: addressUpdate,
  });
  //   delete route
  fastify.route({
    schema: addressDeleteSchema,
    method: "DELETE",
    url: "/delete/:id",
    handler: addressDelete,
  });
  done();
};

module.exports = addressRoute;
