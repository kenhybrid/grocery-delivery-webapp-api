import {
  categoriesAll,
  categorySort,
  categoryCreate,
  categoryDelete,
  categoryUpdate
} from "../controllers/categoryController";
import {
  allCategoriesSchema,
  createCategorySchema,
  sortCategorySchema,
  categoryDeleteSchema,
  updateCategorySchema
} from "../schemas/outputcshema/categorySchema";
const categoryRoute = async (fastify, opts, done) => {
  // get all categories
  fastify.route({
    schema: allCategoriesSchema,
    method: "GET",
    url: "/",
    handler: categoriesAll,
  });
  // create categories route
  fastify.route({
    schema: createCategorySchema,
    method: "POST",
    url: "/create",
    handler: categoryCreate,
  });
  //   sort category route
  fastify.route({
    schema: sortCategorySchema,
    method: "GET",
    url: "/sort/:id",
    handler: categorySort,
  });
//   update
fastify.route({
    schema: updateCategorySchema,
    method: "PUT",
    url: "/update/:id",
    handler: categoryUpdate,
  });
  //  category delete route
  fastify.route({
    schema: categoryDeleteSchema,
    method: "DELETE",
    url: "/delete/:id",
    handler: categoryDelete,
  });
  done();
};

module.exports = categoryRoute;
